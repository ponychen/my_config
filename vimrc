set nocompatible
filetype off
syntax on
set cursorline
set nu
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent
colorscheme elflord

" show file path and encoding
set modeline
set ls=2

"------------------------------------------------------------------------------
" Search
"------------------------------------------------------------------------------
set incsearch   " incremental search
" When 'ignorecase' and 'smartcase' are both on, if a pattern contains an uppercase letter, it is case sensitive, otherwise, it is not. For example, /The would find only "The", while /the would find "the" or "The" etc.
set ignorecase  " ignore case when searching
set smartcase
set hlsearch    " highlight serach result

"------------------------------------------------------------------------------
" Command Mode
"------------------------------------------------------------------------------
set wildmenu
set history=5000

if has("statusline")
    set statusline=%<%f\ %h%m%r%=%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\ \"}%k\ %-14.(%l,%c%V%)\ %P
endif

" SmartIndent
im  :<CR> :<CR><TAB>
" Trim Trailing Whitespace
autocmd BufWritePre *.py normal m`:%s/\s\+$//e``

" Use F11 to turn on/off comment in /* ~  */
let s:vimcomment = "0"
func Comment_Toggle()
    if s:vimcomment == "0"
        set foldmethod=marker
        set foldmarker=/*,*/
        let s:vimcomment = "1"
    else
        set foldmethod=syntax
        let s:vimcomment = "0"
    endif
endf
map <F11> : call Comment_Toggle()<CR>

" Use F12 to paste content
set pastetoggle=<F12>

" Use mouse in all mode
" set mouse=a

" set backspace key for mac
set backspace=indent,eol,start

" tagbar
" let g:tagbar_ctags_bin='/usr/local/bin/ctags'
nnoremap <F5> :TagbarToggle<CR>
" set focus to TagBar when opening it
let g:tagbar_autofocus = 1

" Use Vundle to MANAGE vim plugins
" ====================================================================
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" vim-airline
" https://github.com/bling/vim-airline
Plugin 'bling/vim-airline'

" vim-fugitive
" https://github.com/tpope/vim-fugitive
Plugin 'tpope/vim-fugitive'

" All of your Plugins must be added before the follwing line
call vundle#end()
filetype plugin indent on

" enable Smarter tabline
let g:airline#extensions#tabline#enabled = 1
